// Imports
import _ from 'lodash';
import * as Chart from 'chart.js';
import * as moment from 'moment';
import * as SimpleSwitch from "a-simple-switch";
window['Chart'] = Chart;
window['Chartd'] = Chart;

// Add/Remove HTML parts
// gauge and history
const thermoHtml = '<div class="box" id="gauge"> <canvas id="doughnutChart"></canvas> <div class="temp"></div> </div><div class="history"><h2>History</h2><canvas id="historyChart"></canvas></div>'
document.getElementsByClassName("markdown-body")[0].firstElementChild.insertAdjacentHTML("afterEnd", thermoHtml)

// Switch
const switchHtml = '<div id="unitswitch">°F<input type="checkbox" name="unit-switch" id="unit-switch" />°C</div>'
const title = document.getElementsByClassName("markdown-body")[0].firstElementChild
title.remove()
const newTitle = '<div class="box" id=title><h1>Piscine</h1>' + switchHtml + '</div>'
const body = document.getElementsByClassName("markdown-body")[0]
body.insertAdjacentHTML("afterBegin", newTitle)

let uselessNode = document.getElementById("log")
uselessNode.previousElementSibling.remove()
uselessNode.remove()

// advanced
const table = document.getElementById("states").remove()
const form = document.getElementsByTagName("form")[0].remove()
const ps = document.getElementsByTagName("p")[0].remove()
const h2titles = document.getElementsByTagName("h2")
h2titles[2].remove()
h2titles[1].remove()
const advancedNode = '<div class="box" id="advanced"><h2>Advanced</h2> <h3>Actions</h3><div id="actions"><div class="action-row"><button id="restart">Restart</button></div><div class="action-row"><button id="shutdown">Shutdown</button></div></div><h3>OTA update</h3><div id="ota"><form method="POST" action="/update" enctype="multipart/form-data"><div><input type="file" name="update"><input id="submit" type="submit" value="Update"></div></form></div></div>'
body.insertAdjacentHTML("beforeEnd", advancedNode)

// Functions
function fetchData() {
  var request = new XMLHttpRequest();
  request.open('GET', '/text_sensor/history', false);  // `false` makes the request synchronous
  request.send(null);
  let historyData;
  if (request.status === 200) {
  	let response = JSON.parse(request.responseText);
	historyData = JSON.parse(response.value)
  }
  return historyData
}


function renderUI(unit, historyData) {

    let lastData = historyData[historyData.length - 1];
    let history_data = []
    let firstDataDate = moment().subtract(historyData.length * 5, 'minutes');
    var newtime = firstDataDate;
    let min = 10
    let max = 35

    if (unit == "F"){
      for (var i = 0; i < historyData.length; i++) {
	let value = historyData[i] * (9.0/5.0) + 32.0;
	history_data.push({x: newtime.valueOf(), y: value.toFixed(1)});
	newtime = newtime.add(5, 'minutes')
      }
      min = min * (9.0/5.0) + 32.0;
      max = max * (9.0/5.0) + 32.0;
      lastData = lastData * (9.0/5.0) + 32.0;
    } else {
      for (var i = 0; i < historyData.length; i++) {
	history_data.push({x: newtime.valueOf(), y: historyData[i]});
	newtime = newtime.add(5, 'minutes')
      }
    }

    historyChart.config.options.scales.yAxes[0].ticks.suggestedMin = min;
    historyChart.config.options.scales.yAxes[0].ticks.suggestedMax = max;

    let formatData = {
	datasets: [{
	  label: 'Temp °' + unit,
	  data: history_data,
	  borderColor: "#00fffb",
	  fill: false,
	  pointRadius: 0,
	  backgroundColor: "#bef4fb",
	  radius: 0,
	  hitRadius: 10,
	  pointBorderWidth: 0,
	  borderWidth: 5,
	  hoverRadius: 1,
	 }]
    }
    historyChart.data = formatData;
    historyChart.update()

    // LastData
    let formatLastData = {
      labels: [],
    responsive: true,
      legend: {
	      display: false,
      },
      datasets: [
	{
        data: [lastData.toFixed(1), max + min - lastData.toFixed(1) ],
        backgroundColor: ['#01befe', 'white'],
        borderColor: ['#00fffb', 'white'],
        borderWidth: 0,
        hoverBackgroundColor: ['#01befe', 'white'],
	hoverBorderColor: ['#00fffb', 'white']
        }]
    }
    doughnutChart.data = formatLastData;
    doughnutChart.update()
    const gauge = document.querySelector("#gauge");
    gauge.querySelector(".temp").textContent = `${lastData.toFixed(1).toString()}°${unit}`;

}


// Init Javascripts
// History
const config = {
  type: 'line',
  options: {
    responsive: true,
    legend: {
	    display: false,
    },
    tooltips: {
	    titleFontSize: 30,
	    bodyFontSize: 30,
	    footerFontSize: 30,
    },
    scales: {
      xAxes: [{
        type: 'time',
	ticks: {
	  fontSize: 30,
	  maxTicksLimit: 7,
	  display: true,
	},
	time: {
		round: 'seconds',
		unit: 'day',
                tooltipFormat: 'YYYY MMMM DD - hh:mm',
	}
      }],
      yAxes: [{
	type: 'linear',
	display: 'true',
	     gridLines : {
		     display: true
	     },
        ticks: {
	  fontSize: 30,
          suggestedMin: 15,
          suggestedMax: 35
        }
      }]
    }
  }
}
const historyChart = new Chart(
  document.getElementById('historyChart'),
  config
);
const historyData = fetchData()


const configd = {
  type: 'doughnut',
  options: {
    tooltips : {
	    enabled: false
    },
    cutoutPercentage: 80,
    rotation: 1 * Math.PI,
    circumference: 1 * Math.PI
  }
}
const doughnutChart = new Chartd(
    document.getElementById('doughnutChart'),
    configd
);
// Unit Switch
SimpleSwitch.init();
var unit = "F"
let unitSwitchEl = document.getElementById("unit-switch");
unitSwitchEl.addEventListener('change', (event) => {
  if (unitSwitch.checked === false) {
    unit = "F"
  } else{
    unit = "C"
  }
  renderUI(unit, historyData)
})

const unitSwitch = new SimpleSwitch.Switch({
    element: unitSwitchEl,
    material: true,
    matchSizeToFont: true
});

// Render

renderUI(unit, historyData)


//

const restartButton = document.getElementById('restart');
restartButton.addEventListener('click', function () {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", '/switch/pool_sensor_restart/toggle', true);
  xhr.send();
});

const shutdownButton = document.getElementById('shutdown');
shutdownButton.addEventListener('click', function () {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", '/switch/pool_sensor_shutdown/toggle', true);
  xhr.send();
})
